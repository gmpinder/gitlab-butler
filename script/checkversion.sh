#!/bin/bash

cli_version=$(tomlq package.version -f gitlab-butler-cli/Cargo.toml)
lib_version=$(tomlq package.version -f gitlab-butler-lib/Cargo.toml)
lib_from_cli_version=$(tomlq dependencies.gitlab-butler-lib.version -f gitlab-butler-cli/Cargo.toml)

echo "[Cargo] gitlab-butler-cli.package.version: $cli_version"
echo "[Cargo] gitlab-butler-lib.package.version: $lib_version"
echo "[Cargo] gitlab-butler-cli.dependencies.gitlab-butler-lib.version: $lib_from_cli_version"

if [[ "$CI" != "" ]]; then
    echo "[ Env ] CI_COMMIT_TAG: $CI_COMMIT_TAG"
    if [[ "$CI_COMMIT_TAG" != "$lib_version" ]]; then
        echo "! ! ! ! ! ! ! ! ! ! ! ! ! !"
        echo "! ERROR: version mismatch !"
        echo "! ! ! ! ! ! ! ! ! ! ! ! ! !"
        exit 1
    fi
else
    echo "[ Env ] no CI, do not publish!"
fi

if [[ "$cli_version" != "$lib_version" ]] || [[ "$lib_version" != "$lib_from_cli_version" ]]; then
    echo "ERROR: version mismatch"
    exit 2
else
    echo "* * * * * * * * * * * *"
    echo "* OK: version matches *"
    echo "* * * * * * * * * * * *"
fi
