// Copyright 2020 Matteo Bertini <matteo@naufraghi.net>

pub mod client;
pub mod entities;
pub mod issues;
pub mod merge_requests;

pub mod personal {
    use anyhow::Result;
    use log::{debug, error};

    use crate::client::Client;
    use crate::entities::PersonalAccessToken;

    impl PersonalAccessToken {
        pub fn list(client: &Client) -> Result<Vec<PersonalAccessToken>> {
            let api_path = "api/v4/personal_access_tokens";
            debug!("Ready to query {:?}", api_path);
            let response = client.get(api_path)?;
            let de = &mut serde_json::Deserializer::from_reader(response);
            let result: Result<Vec<PersonalAccessToken>, _> = serde_path_to_error::deserialize(de);
            match result {
                Ok(personal_tokens) => Ok(personal_tokens),
                Err(err) => {
                    error!("Path: {}", err.path().to_string());
                    error!("Body: {:?}", client.get(api_path)?.bytes()?);
                    Err(err.into_inner().into())
                }
            }
        }
    }
}
