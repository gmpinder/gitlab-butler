_list:
	@just --list

bootstrap:
	cargo install cargo-workspaces

_build:
	cargo build --release

_deploy: _build
	cp target/release/gitlab-butler docker/

docker-build: _deploy
	docker build --tag gitlab-butler-test docker/

docker-test:
	just docker-run

docker-run ARGS="--help":
	docker run --rm -it gitlab-butler-test /run/gitlab-butler {{ARGS}}

docker-build-test: docker-build docker-test

bump-version PART="minor":
	cargo bump {{PART}}

cog:
	cog -r README.md

backport TARGET:
	git lab issue get -x "just _backport-issue {{TARGET}}"

_backport-issue TARGET:
	git lab issue new "$issue_title ({{TARGET}})" -d "Backport of issue $issue_reference" -x "just _backport-mr {{TARGET}}"

_backport-mr TARGET:
	git branch $issue_slug origin/{{TARGET}}
	git push origin $issue_slug
	git lab mr new "Resolve \"$issue_title\"" -d "Closes $issue_reference" -s $issue_slug -t {{TARGET}}

publish OPTIONS="--dry-run":
	# deps first
	(cd gitlab-butler-lib && cargo publish {{OPTIONS}})
	(cd gitlab-butler-cli && cargo publish {{OPTIONS}})
