# Gitlab butler cli contribution guidelines

## Idea

The project plans to produce a usable CLI for Gitlab, usability comes first.

This project is also an experiment: instead of being one of my side projects
_in-the-search-for-perfection_, `gitlab-butler` is a GTD applied to software
development; instead of postponing the development of a feature until a
"perfect" structure was created, in this project I preferred to work on a bit
uglier code and add functionality until refactoring is really necessary, not
anticipating a change but waiting for the compromise to be positive.

## Notice

This is a personal project, feel free to contribute some feedback or code, but
don't expect a timely response, I develop "gitlab-butler" in my spare time.  If
the proposal is not trivial, expect even more latency, as I need time to assess
whether I am willing to include some complex code which I will then have to
maintain.

## Plan


The main goal of `gitlab-butler` is to become a tool used day to day by
developers using Gitlab. There are various alternatives but I would like to
write a little more about Rust :)

### Missing parts

- [ ] Mutation
- [ ] Tests

### Priority

1. Usability
2. Backwards compatibility (cli semver after 1.0 ideally)
3. Modularity

## License

Licensed under either of
 * Apache License, Version 2.0 ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)
at your option.

### Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you shall be dual licensed as above, without any
additional terms or conditions.
