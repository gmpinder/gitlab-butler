// Copyright 2020 Matteo Bertini <matteo@naufraghi.net>

use anyhow::{bail, Result};
use clap_verbosity_flag::Verbosity;
use structopt::StructOpt;
use url::Url;

use gitlab_butler_lib::client::Client;
use gitlab_butler_lib::entities::*;

pub mod git;
pub mod issue;
pub mod mr;

use issue::{IssueEdit, IssueSubcommand, NewIssue};
use milestone::{MilestoneSubcommand, Milestones};
use mr::{MergeSubcommand, NewMergeRequest};

// Command line options
#[derive(Debug, StructOpt)]
#[structopt(rename_all = "kebab-case")]
struct Cli {
    #[structopt(long, short = "s", env = "GITLAB_BUTLER_API_SERVER")]
    /// Host to be contacted (defaults to host from CI_API_V4_URL)
    api_server: Url,
    // -------------------------------------------------------------------------
    #[structopt(
        long,
        short = "t",
        env = "GITLAB_BUTLER_PRIVATE_TOKEN",
        hide_env_values = true
    )]
    /// User private token
    private_token: String,
    // -------------------------------------------------------------------------
    #[structopt(subcommand)]
    cmd: Command,
    // Quick and easy logging setup
    #[structopt(flatten)]
    verbosity: Verbosity,
}

#[derive(StructOpt, Debug)]
#[structopt(rename_all = "kebab-case")]
enum Command {
    #[structopt(name = "mr")]
    /// Manage merge requests
    Merges(MergeSubcommand),
    #[structopt(name = "issue")]
    /// Manage issues
    Issues(IssueSubcommand),
    #[structopt(name = "milestone")]
    /// Manage milestones
    Milestones(MilestoneSubcommand),
    #[structopt(name = "project")]
    /// Manage projects
    Projects(ProjectSubcommand),
    #[structopt(name = "install")]
    /// Install `git-lab` alias (let call `git lab <command>`)
    Install,
    #[structopt(name = "get-token")]
    /// Use ssh to get a valid token for gitlab
    GetToken(GetTokenSubcommand),
}

mod milestone {
    use structopt::StructOpt;

    use gitlab_butler_lib::entities::ProjectMilestone;

    pub fn print_milestones(milestones: Vec<ProjectMilestone>) {
        println!(
            "Found {} milestone{}:",
            milestones.len(),
            if milestones.len() == 1 { "" } else { "s" }
        );
        for milestone in milestones {
            println!("- {}", milestone.title);
        }
    }
    #[derive(StructOpt, Debug)]
    #[structopt(rename_all = "kebab-case")]
    pub struct MilestoneSubcommand {
        #[structopt(subcommand)]
        pub cmd: Milestones,
    }
    #[derive(StructOpt, Debug)]
    #[structopt(rename_all = "kebab-case")]
    pub enum Milestones {
        #[structopt(name = "list")]
        /// List milestones
        ListMilestones(ListMilestones),
    }
    #[derive(StructOpt, Debug)]
    #[structopt(rename_all = "kebab-case")]
    pub struct ListMilestones {
        #[structopt(long, short = "p", env = "GITLAB_BUTLER_PROJECT")]
        /// Project name or id (defaults to CI_PROJECT_PATH)
        pub project: String,
    }
}

mod pipeline {
    use gitlab_butler_lib::entities::Pipeline;

    pub fn print_pipelines(pipelines: Vec<Pipeline>) {
        println!(
            "Found {} pipeline{}:",
            pipelines.len(),
            if pipelines.len() == 1 { "" } else { "s" }
        );
        for pipeline in pipelines.iter().take(5) {
            match pipeline.status.as_str() {
                "failed" => bunt::println!(
                    "- {} #{} {$red}failed{/$}",
                    pipeline.updated_at,
                    pipeline.id
                ),
                "success" => bunt::println!(
                    "- {} #{} {$green}success{/$}",
                    pipeline.updated_at,
                    pipeline.id
                ),
                rest => bunt::println!("- {} #{} {}", pipeline.updated_at, pipeline.id, rest),
            };
        }
    }
}

mod ssh {
    use anyhow::{bail, Result};
    use slug::slugify;
    use std::io::Read;
    use std::string::String;

    pub(crate) fn get_gitlab_token(server: &str, ttl_days: Option<usize>) -> Result<String> {
        let hostname = hostname::get()?;
        let hostname_lossy = slugify(hostname.to_string_lossy());
        let ttl_days = ttl_days.as_ref().map_or(String::new(), usize::to_string);
        // Example response:
        //   Token:   sf_ZWyFErk-KpAwphdeU8uu
        //   Scopes:  api
        //   Expires: 2021-01-16
        use ssh2::Session;
        use std::net::TcpStream;
        // Connect to the SSH server
        let tcp = TcpStream::connect(server)?;
        let mut sess = Session::new()?;

        sess.set_tcp_stream(tcp);
        sess.handshake()?;
        sess.userauth_agent("git")?;
        sess.set_blocking(true);
        let mut channel = sess.channel_session()?;
        channel.exec(&format!(
            "personal_access_token gitlab-butler-{hostname_lossy} api {ttl_days}"
        ))?;
        let mut s = String::new();
        channel.read_to_string(&mut s)?;
        for mut v in s.lines().map(|l| l.split_ascii_whitespace()) {
            if let (Some(key), Some(value)) = (v.next(), v.next()) {
                if key == "Token:" {
                    return Ok(value.to_string());
                }
            }
        }
        let mut e = String::new();
        channel.stderr().read_to_string(&mut e)?;
        eprintln!("<eresponse>");
        eprint!("{e}");
        eprintln!("</eresponse>");
        channel.wait_close()?;
        eprintln!("exit-code: {}", channel.exit_status()?);
        bail!(e)
    }
    #[test]
    fn test_ssh_token() {
        match get_gitlab_token("localhost:22", Some(7)) {
            Ok(_) => {
                // Ok, we have a valid key in this environment"
            }
            Err(e) => {
                if let Some(err) = e.downcast_ref::<ssh2::Error>() {
                    // without a valid key we have an LIBSSH2_ERROR_AUTHENTICATION_FAILED=-18 error
                    assert_eq!(ssh2::ErrorCode::Session(-18), err.code(), "{:?}", err);
                } else if let Some(err) = e.downcast_ref::<std::io::Error>() {
                    // somehow disconnected from the server
                    match err.kind() {
                        std::io::ErrorKind::TimedOut => {}
                        _ => eprintln!("Unknown error: {:?}", err),
                    }
                } else {
                    unreachable!("{:?}", e);
                }
            }
        };
    }
}

#[derive(StructOpt, Debug)]
#[structopt(rename_all = "kebab-case")]
struct ProjectSubcommand {
    #[structopt(subcommand)]
    cmd: Projects,
}

#[derive(StructOpt, Debug)]
#[structopt(rename_all = "kebab-case")]
struct GetTokenSubcommand {
    #[structopt(name = "host:port", env = "GITLAB_BUTLER_SSH_HOST")]
    /// Host and port for ssh, defaults to the host:port used by git
    ssh_host: String,
    #[structopt(long, short = "t")]
    /// Token time to live in days (default: no expiry date)
    ttl_days: Option<usize>,
}

impl From<NewMergeRequest> for CreateMergeRequest {
    /// Create a new `CreateMergeRequest` struct.
    /// NOTE: This struct has no milestone_id because it must be resolved from the name.
    fn from(mr: NewMergeRequest) -> CreateMergeRequest {
        CreateMergeRequest {
            project_id: mr.project,
            title: mr.title,
            description: mr.description,
            labels: mr.labels,
            source_branch: mr.source_branch,
            target_branch: mr.target_branch,
            ..Default::default()
        }
    }
}

#[derive(StructOpt, Debug)]
#[structopt(rename_all = "kebab-case")]
enum Projects {
    #[structopt(name = "list")]
    /// List project
    ListProjects(ListProjects),
}

impl From<NewIssue> for CreateIssue {
    /// Create a new `CreateIssue` struct.
    /// NOTE: This struct has no milestone_id because it must be resolved from the name.
    fn from(issue: NewIssue) -> CreateIssue {
        CreateIssue {
            project_id: issue.project,
            title: issue.title,
            description: issue.description,
            labels: issue.labels,
            ..Default::default()
        }
    }
}

impl From<IssueEdit> for EditIssue {
    /// Create a new `EditIssue` struct.
    fn from(issue: IssueEdit) -> EditIssue {
        EditIssue {
            title: issue.title,
            description: issue.description,
            labels: issue.labels,
            state_event: issue.state_event,
            ..Default::default()
        }
    }
}

#[derive(StructOpt, Debug)]
#[structopt(rename_all = "kebab-case")]
struct ListProjects {
    #[structopt(long, short = "s")]
    search: Option<String>,
    #[structopt(long, short = "n", default_value = "100")]
    limit: usize,
}

fn import_env() {
    dotenv::dotenv().ok();
    dotenv::from_filename(".private.env").ok();
    let _import_env = |source, dest| {
        if let Some(api_server) = std::env::var_os(source) {
            if std::env::var_os(dest).is_none() {
                std::env::set_var(dest, &api_server);
            }
        }
    };
    _import_env("CI_API_V4_URL", "GITLAB_BUTLER_API_SERVER");
    _import_env("CI_PROJECT_PATH", "GITLAB_BUTLER_PROJECT");
    if std::env::var_os("GITLAB_BUTLER_SOURCE_BRANCH").is_none() {
        if let Some(name) = git::get_branch_name() {
            std::env::set_var("GITLAB_BUTLER_SOURCE_BRANCH", &name)
        }
    }
    if let Some(git::RepoRemote {
        ssh_host,
        api_server,
        project,
    }) = git::gitlab_target_from_remote()
    {
        if std::env::var_os("GITLAB_BUTLER_SSH_HOST").is_none() {
            std::env::set_var("GITLAB_BUTLER_SSH_HOST", &ssh_host);
        }
        if std::env::var_os("GITLAB_BUTLER_API_SERVER").is_none() {
            std::env::set_var("GITLAB_BUTLER_API_SERVER", &api_server);
        }
        if std::env::var_os("GITLAB_BUTLER_PROJECT").is_none() {
            std::env::set_var("GITLAB_BUTLER_PROJECT", &project);
        }
    };
}

fn setup_env_logger(verbosity: &Verbosity) -> Result<()> {
    use log::Level;
    let level_filter = verbosity
        .log_level()
        .unwrap_or(Level::Warn)
        .to_level_filter();
    env_logger::Builder::new()
        .filter(Some("gitlab_butler"), level_filter)
        .filter(None, Level::Warn.to_level_filter())
        .try_init()?;
    Ok(())
}

fn main() -> Result<()> {
    import_env();
    let args = Cli::from_args();
    setup_env_logger(&args.verbosity)?;

    let client = Client::new(args.api_server, args.private_token.as_bytes())?;

    fn print_projects(projects: Vec<Project>) {
        println!(
            "Found {} project{}:",
            projects.len(),
            if projects.len() == 1 { "" } else { "s" }
        );
        for project in projects {
            println!("- {}", project.path_with_namespace);
        }
    }

    match args.cmd {
        Command::Install => {
            if cfg!(windows) {
                bail!("Unsupported on Windows");
            } else {
                use std::env;
                use std::path::PathBuf;
                println!("Install `git-lab` alias in `~/.local/bin`");
                #[allow(deprecated)]
                let target: Option<PathBuf> = env::home_dir().map(|mut home| {
                    home.push(".local/bin/git-lab");
                    home
                });
                if let Some(target) = target {
                    let current_exe = env::current_exe()?;
                    if target.exists() {
                        if target.read_link()? == current_exe {
                            println!(
                                "Link already exists: {} -> {}",
                                target.to_string_lossy(),
                                current_exe.to_string_lossy()
                            );
                        } else {
                            bail!(
                                "Link already exists and points a different file: {} -> {}",
                                target.to_string_lossy(),
                                current_exe.to_string_lossy()
                            );
                        }
                    } else {
                        use std::os::unix::fs;
                        fs::symlink(&current_exe, &target)?;
                        println!(
                            "Link created: {} -> {}",
                            target.to_string_lossy(),
                            current_exe.to_string_lossy()
                        );
                    }
                };
                Ok(())
            }
        }
        Command::GetToken(subcommand) => {
            // TODO: should try to use the API, on error, try to create a token
            if PersonalAccessToken::list(&client).is_ok() {
                println!("The current token is valid, nothing to do!");
            } else {
                // TODO: where should we persist the token? git config? `.gitlab.env`?
                let token = ssh::get_gitlab_token(&subcommand.ssh_host, subcommand.ttl_days)?;
                println!("Save the token in the env GITLAB_BUTLER_PRIVATE_TOKEN={token}");
            }
            Ok(())
        }
        Command::Merges(subcommand) => mr::command(&client, subcommand.cmd),
        Command::Issues(subcommand) => issue::command(&client, subcommand.cmd),
        Command::Milestones(subcommand) => match subcommand.cmd {
            Milestones::ListMilestones(milestone) => {
                let milestones = ProjectMilestone::in_project(&client, &milestone.project)?;
                milestone::print_milestones(milestones);
                Ok(())
            }
        },
        Command::Projects(subcommand) => match subcommand.cmd {
            Projects::ListProjects(list_projects) => {
                let projects = Project::list(&client, &list_projects.search, list_projects.limit)?;
                print_projects(projects);
                Ok(())
            }
        },
    }
}
