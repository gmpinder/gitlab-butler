use anyhow::{bail, Result};
use git2::Repository;
use git_url_parse::GitUrl;
use log::{error, info};
use std::env;
use std::path::Path;

#[derive(Debug)]
pub struct RepoRemote {
    pub ssh_host: String,
    pub api_server: String,
    pub project: String,
}

pub fn repo() -> Result<Repository> {
    let cwd = env::current_dir()?;
    if let Ok(repo) = Repository::discover(&Path::new(&cwd)) {
        return Ok(repo);
    }
    bail!(
        "Unable to find a repository up from {}",
        cwd.to_string_lossy()
    );
}

pub fn get_branch_name() -> Option<String> {
    let repo = repo().ok()?;
    if repo.is_bare() {
        error!("Unable to work on a bare repo");
        return None;
    }
    let head = repo.head().ok();
    if let Some(name) = head.and_then(|h| {
        if !h.is_remote() {
            info!("Current branch is not a remote");
        }
        h.shorthand().map(|h| h.to_string())
    }) {
        Some(name)
    } else {
        error!("Unable to detect current branch name");
        None
    }
}

fn parse_git_remote(remote: &str) -> Option<RepoRemote> {
    let url = GitUrl::parse(remote)
        .map_err(|e| error!("{}", e))
        .ok()
        .filter(|url| url.host.is_some());

    url.map(|url| RepoRemote {
        ssh_host: format!(
            "{}:{}",
            url.host.as_ref().expect("has_host tested above"),
            url.port.unwrap_or(22)
        ),
        api_server: format!("https://{}", url.host.expect("has_host tested above")),
        project: url
            .path
            .trim_start_matches('/')
            .trim_end_matches(".git")
            .to_string(),
    })
}

pub fn gitlab_target_from_remote() -> Option<RepoRemote> {
    let repo = repo().map_err(|e| error!("{}", e)).ok()?;
    let mut remotes: Vec<String> = repo
        .remotes()
        .map_err(|e| error!("{}", e)) // notify errors and continue
        .ok()?
        .iter()
        .filter_map(|r| r.map(|r| r.to_string()))
        .collect();
    remotes.sort_by_key(|r| r != "origin"); // origin remote comes first
    for name in remotes {
        let remote = repo.find_remote(&name).map_err(|e| error!("{}", e)).ok()?;
        let url = remote.url()?;
        if let Some(rem) = parse_git_remote(&url).filter(|r| r.api_server.contains("gitlab")) {
            return Some(rem);
        }
    }
    None
}

#[test]
fn test_gitlab_target_from_remote() {
    gitlab_target_from_remote().expect("gitlab_target_from_remote");
}
